#!/bin/bash -e

ip=$(hostname -I | awk '{print $2}')
sudo apt install unzip

cd /usr/local/bin
sudo curl -o consul.zip https://releases.hashicorp.com/consul/1.6.0/consul_1.6.0_linux_amd64.zip
unzip consul.zip
sudo rm -f consul.zip
sudo mkdir -p /etc/consul.d/scripts
sudo mkdir /var/consul
sudo touch /etc/consul.d/config.json

cat > /etc/consul.d/config.json <<- EOF
{ 
   "bootstrap_expect":2,
   "bind_addr": "$ip",
   "client_addr":"0.0.0.0",
   "datacenter":"Us-Central",
   "data_dir":"/var/consul",
   "domain":"consul",
   "enable_script_checks":true,
   "dns_config":{ 
      "enable_truncate":true,
      "only_passing":true
   },
   "enable_syslog":true,
   "encrypt":"goplCZgdmOFMZ2Q43To0jw==",
   "leave_on_terminate":true,
   "log_level":"INFO",
   "rejoin_after_leave":true,
   "server":true,
   "start_join":[ 
      "192.168.56.101",
      "192.168.56.102"
   ],
   "ui":true
}
EOF

sudo touch /etc/systemd/system/consul.service

cat > /etc/systemd/system/consul.service <<- EOF
[Unit]
Description=Consul Startup process
After=network.target
 
[Service]
Type=simple
ExecStart=/bin/bash -c '/usr/local/bin/consul agent -config-dir /etc/consul.d/'
TimeoutStartSec=0
 
[Install]
WantedBy=default.target
EOF

sudo systemctl daemon-reload
sudo systemctl start consul
